FROM maven:3.6-jdk-11

WORKDIR /app

COPY pom.xml .

# Immediately download all dependencies. This results in a slightly longer image build the first time, but then
# Docker caches the intermediate image and does not execute it again until pom.xml changes.
RUN mvn dependency:go-offline

COPY . .

RUN mvn compile

CMD ["mvn", "spring-boot:run"]
