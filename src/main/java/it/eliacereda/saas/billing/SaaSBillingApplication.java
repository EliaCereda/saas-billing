package it.eliacereda.saas.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main Spring Configuration class for the application.
 * <p>
 * It enables Spring Boot auto-configuration of the Application Context and automatic component scanning in the
 * {@link it.eliacereda.saas.billing} package and all its subpackages. Furthermore it defines the static
 * {@link #main(String[])} method to initialize and start the application.
 */
@SpringBootApplication
public class SaaSBillingApplication {
    /**
     * Initializes and starts the Spring application.
     *
     * @param args the command line arguments used to start the application.
     */
    public static void main(String[] args) {
        SpringApplication.run(SaaSBillingApplication.class, args);
    }
}
