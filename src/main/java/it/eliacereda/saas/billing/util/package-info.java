/**
 * Contains utility classes for the implementation of {@link it.eliacereda.saas.billing.model.AttributesComparable}.
 */
package it.eliacereda.saas.billing.util;
