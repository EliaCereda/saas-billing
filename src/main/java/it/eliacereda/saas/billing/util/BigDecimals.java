package it.eliacereda.saas.billing.util;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class containing {@code static} utility methods for operating on {@link BigDecimal} objects.
 *
 * @see Objects
 */
public class BigDecimals {

    private BigDecimals() { throw new AssertionError("No instances of this class should exist."); }

    /**
     * Returns {@code true} if the arguments are equal to each other and {@code false} otherwise, considering equal two
     * {@link BigDecimal} objects that are equal in value but different in scale (like 2.0 and 2.00).
     * <p>
     * This method can be safely called even if {@code a} or {@code b} are {@code null}: it returns {@code true} if both
     * are {@code null} and {@code false} otherwise. This methods relates to {@link BigDecimal#compareTo(BigDecimal)} as
     * {@link Objects#equals(Object, Object)} relates to {@link Object#equals(Object)}.
     *
     * @param a a {@link BigDecimal} object.
     * @param b a {@link BigDecimal} object to compare with {@code a}.
     * @return {@code true} if the arguments are equal to each other and {@code false} otherwise.
     * @see BigDecimal#compareTo(BigDecimal)
     * @see Objects#equals(Object, Object)
     */
    public static boolean equalsIgnoringScale(BigDecimal a, BigDecimal b) {
        //noinspection NumberEquality
        return (a == b) || (a != null && b != null && a.compareTo(b) == 0);
    }
}
