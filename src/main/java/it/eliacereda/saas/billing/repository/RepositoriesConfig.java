package it.eliacereda.saas.billing.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/** Spring Configuration class that sets the base class used to instantiate the repositories. */
@Configuration
@EnableJpaRepositories(repositoryBaseClass = BaseRepositoryImpl.class)
public class RepositoriesConfig {

}
