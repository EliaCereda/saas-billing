package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.BillingPeriod;
import it.eliacereda.saas.billing.model.Plan;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Repository for Plan entities.
 * <p>
 * It implements the Data Access Object pattern and provides CRUD operations and more complex queries for its entities.
 */
@Repository
public interface PlanRepository extends BaseRepository<Plan, Long> {
    /**
     * Finds all the Plans with a given billing period.
     * @param billingPeriod the billing period to find.
     * @return the set of Plans with the given billing period.
     */
    Set<Plan> findAllByBillingPeriod(BillingPeriod billingPeriod);
}
