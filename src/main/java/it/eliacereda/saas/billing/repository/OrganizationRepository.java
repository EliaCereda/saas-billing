package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.Organization;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for Organization entities.
 * <p>
 * It implements the Data Access Object pattern and provides CRUD operations and more complex queries for its entities.
 */
@Repository
public interface OrganizationRepository extends BaseRepository<Organization, Long> {
    /**
     * Finds the Organization with a given VAT Identifier, if it exists.
     * @param vatIdentifier a VAT Identifier to find.
     * @return the Organization with the given VAT Identifier or {@code Optional.empty()} if none exists.
     * @throws IncorrectResultSizeDataAccessException if more than one Organization with the same VAT Identifier exists.
     */
    Optional<Organization> findByVatIdentifier(String vatIdentifier);
}
