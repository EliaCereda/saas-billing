package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.Customer;
import org.springframework.stereotype.Repository;

/**
 * Repository for Customer entities.
 * <p>
 * It implements the Data Access Object pattern and provides CRUD operations and more complex queries for its entities.
 */
@Repository
public interface CustomerRepository extends BaseRepository<Customer, Long> {

}
