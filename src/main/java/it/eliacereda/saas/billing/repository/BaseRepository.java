package it.eliacereda.saas.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Base interface from which all repositories inherit. It allows to define custom operations on top of those offered by
 * {@link JpaRepository} and make them available to all repositories.
 */
@NoRepositoryBean
public interface BaseRepository<T, ID> extends JpaRepository<T, ID> {
    /**
     * Flushes all pending changes to the database and clears the persistence context, causing all managed
     * entities to become detached.
     * <p>
     * This is useful in the test suite to be able to clear the cache and consistently hit the database. It should not
     * normally be used outside of the tests.
     */
    void flushAndClear();
}
