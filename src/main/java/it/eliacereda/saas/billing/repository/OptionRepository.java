package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.Option;
import org.springframework.stereotype.Repository;

/**
 * Repository for Option entities.
 * <p>
 * It implements the Data Access Object pattern and provides CRUD operations and more complex queries for its entities.
 */
@Repository
public interface OptionRepository extends BaseRepository<Option, Long> {

}
