package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.Person;
import org.springframework.stereotype.Repository;

/**
 * Repository for Person entities.
 * <p>
 * It implements the Data Access Object pattern and provides CRUD operations and more complex queries for its entities.
 */
@Repository
public interface PersonRepository extends BaseRepository<Person, Long> {

}
