/**
 * Contains the Spring Data repositories that abstract the specific operations used to access the database from the rest
 * of the project.
 * <p>
 * One repository interface is provided for each entity class, along with a
 * {@link it.eliacereda.saas.billing.repository.BaseRepository} interface that the other inherit. Each repository
 * defines a series of generic CRUD operations and query methods specific to its entity (e.g.
 * {@code findBy{SomeAttribute}OrderBy{AnotherAttribute}}). At runtime, Spring Data automatically provides an
 * implementation of these interfaces by parsing the method names according to a set of rules.
 *
 * @see <a href="https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.details">Spring Data JPA &mdash; Defining Query Methods</a>
 */

package it.eliacereda.saas.billing.repository;
