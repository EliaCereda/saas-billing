package it.eliacereda.saas.billing.model;

/**
 * Compares two entities to check that the values of their attributes are equals.
 */
public interface AttributesComparable {
    /**
     * Compares two entities to check that the values of their attributes are equals.
     * @param o the other entity to be compared.
     * @return {@code true} if all attributes are equal;
     *         {@code false} otherwise.
     */
    boolean attributesEqual(Object o);
}
