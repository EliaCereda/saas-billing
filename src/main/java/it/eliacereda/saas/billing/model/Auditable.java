package it.eliacereda.saas.billing.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * Abstract base class for JPA entities that automatically provides the creation and modification dates attributes
 * and conformance to {@link Identifiable} and {@link AttributesComparable}.
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<ID> implements Identifiable<ID>, AttributesComparable {

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime modifiedDate;

    // Getters and Setters

    /**
     * Returns the time that this entity was first persisted to the database. The value of this attribute is
     * automatically updated by JPA and cannot be modified by users of this class.
     *
     * @return the time that this entity was first persisted to the database.
     */
    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    /**
     * Returns the time that this entity was last modified in the database. The value of this attribute is automatically
     * updated by JPA and cannot be modified by users of this class.
     *
     * @return the time that this entity was last modified in the database.
     */
    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }
}
