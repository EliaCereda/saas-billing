package it.eliacereda.saas.billing.model;

/** Specifies the billing period for a given subscription plan. */
public enum BillingPeriod {

    /** Billing happens once per week. */
    WEEKLY,

    /** Billing happens once per month. */
    MONTHLY,

    /** Billing happens once per year. */
    YEARLY
}
