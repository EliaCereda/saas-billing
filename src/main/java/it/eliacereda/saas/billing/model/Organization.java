package it.eliacereda.saas.billing.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

/**
 * Entity that models an organization that subscribes to this service. In particular it stores the name of the
 * organization and the information needed to bill the subscription, such as the VAT identifier of the organization.
 * <p>
 * All fields of this entity should be non-nullable, but since all customers are stored in a single table this could
 * only be enforced with a complex CHECK constraint that depends on the dtype attribute to decide what attributes should
 * not be {@code null}.
 */
@Entity
public class Organization extends Customer {

    @Column(length = 20)
    private String vatIdentifier;

    private String organizationName;

    // Getters and Setters

    /**
     * Gets the VAT Identifier of this Organization.
     * @return the VAT Identifier of this Organization.
     */
    public String getVatIdentifier() {
        return vatIdentifier;
    }

    /**
     * Sets the VAT Identifier of this Organization.
     * @param vatIdentifier the new VAT Identifier of this Organization.
     */
    public void setVatIdentifier(String vatIdentifier) {
        this.vatIdentifier = vatIdentifier;
    }

    /**
     * Gets the legal name of this Organization.
     * @return the legal name of this Organization.
     */
    public String getOrganizationName() {
        return organizationName;
    }

    /**
     * Sets the legal name of this Organization.
     * @param organizationName the new legal name of this Organization.
     */
    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    // AttributesComparable

    @Override
    public boolean attributesEqual(Object o) {
        if (!super.attributesEqual(o)) return false;
        if (!(o instanceof Organization)) return false;

        Organization that = (Organization) o;
        return Objects.equals(getVatIdentifier(), that.getVatIdentifier()) &&
                Objects.equals(getOrganizationName(), that.getOrganizationName());
    }
}