/**
 * Contains the JPA entities used to map from relational records to Java objects.
 * <p>
 * These are mostly POJO that declare the mapping strategies to be applied using JPA annotations, without implementing
 * any complex behavior themselves. One exception is the {@link it.eliacereda.saas.billing.model.AttributesComparable}
 * interface, that all entity implement and allows to compare two instances of an entity to check that the values of
 * their attributes are equals.
 */

package it.eliacereda.saas.billing.model;
