package it.eliacereda.saas.billing.model;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Entity that models an individual that subscribes to this service. In particular it stores the given and family name
 * and the date of birth of this person.
 * <p>
 * All fields of this entity should be non-nullable, but since all customers are stored in a single table this could
 * only be enforced with a complex CHECK constraint that depends on the dtype attribute to decide what attributes should
 * not be null.
 */
@Entity
public class Person extends Customer {

    private String givenName;

    private String familyName;

    private LocalDate dateOfBirth;

    // Getters and Setters

    /**
     * Gets the given name of this Person.
     * @return the given name of this Person.
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Sets the given name of this Person.
     * @param givenName the new given name of this Person.
     */
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    /**
     * Gets the family name of this Person.
     * @return the family name of this Person.
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Sets the family name of this Person.
     * @param familyName the new family name of this Person.
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * Gets the date of birth of this Person.
     * @return the date of birth of this Person.
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the date of birth of this Person.
     * @param dateOfBirth the new date of birth of this Person.
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    // AttributesComparable

    @Override
    public boolean attributesEqual(Object o) {
        if (!super.attributesEqual(o)) return false;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;
        return Objects.equals(getGivenName(), person.getGivenName()) &&
                Objects.equals(getFamilyName(), person.getFamilyName()) &&
                Objects.equals(getDateOfBirth(), person.getDateOfBirth());
    }
}
