package it.eliacereda.saas.billing.model;

import it.eliacereda.saas.billing.util.Associations;
import it.eliacereda.saas.billing.util.BigDecimals;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Entity that models a subscription plan offered to the customers of this service.
 */
@Entity
@Table(name = "plans")
public class Plan extends Auditable<Long> {
    @Id
    @GeneratedValue(generator = "plan_generator")
    @SequenceGenerator(
            name = "plan_generator",
            sequenceName = "plan_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column(nullable = false)
    private String name;

    @Lob
    @Column(nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private BillingPeriod billingPeriod;

    @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal rate;

    @OneToMany(mappedBy = "subscription", fetch = FetchType.LAZY)
    private Set<Customer> subscribers = new HashSet<>();

    @OneToMany(mappedBy = "plan", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Option> availableOptions = new HashSet<>();

    // Getters and Setters

    public Long getId() {
        return id;
    }

    /** Sets the identifier of this entity. This method is used in the test suite and shouldn't be used in normal code. */
    void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the name of this subscription Plan.
     * @return the name of this subscription Plan.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this subscription Plan.
     * @param name the new name of this subscription Plan.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets a long description of this subscription Plan.
     * @return the long description of this subscription Plan.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the long description of this subscription Plan.
     * @param description the new long description of this subscription Plan.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the billing period of this subscription Plan.
     * @return the billing period of this subscription Plan.
     */
    public BillingPeriod getBillingPeriod() {
        return billingPeriod;
    }

    /**
     * Sets the billing period of this subscription Plan.
     * @param billingPeriod the new billing period of this subscription Plan.
     */
    public void setBillingPeriod(BillingPeriod billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    /**
     * Gets the amount billed per billing period to subscribers of this subscription Plan.
     * @return the amount billed for this subscription Plan.
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the amount billed per billing period to subscribers of this subscription Plan.
     * @param rate the new amount billed for this subscription Plan.
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    // Associations

    /**
     * Returns the set of Customers that currently subscribe to this Plan.
     * <p>
     * The {@link Set} returned by this method cannot be modified directly, use the provided
     * {@link #addSubscriber(Customer)} and {@link #removeSubscriber(Customer)} methods to update the association.
     *
     * @return the set of {@link Customer}s that subscribe to this Plan.
     */
    public Set<Customer> getSubscribers() {
        return Collections.unmodifiableSet(subscribers);
    }

    void _addSubscriber(Customer subscriber) {
        this.subscribers.add(subscriber);
    }

    void _removeSubscriber(Customer subscriber) {
        this.subscribers.remove(subscriber);
    }

    /**
     * Adds a Customer to the set of current subscribers.
     *
     * @param subscriber the Customer that wants to subscribe to this Plan.
     */
    public void addSubscriber(Customer subscriber) {
        subscriber.setSubscription(this);
    }

    /**
     * Removes a Customer from the set of current subscribers.
     *
     * @param subscriber the Customer that should be unsubscribed.
     * @throws IllegalArgumentException if the Customer is not a current subscriber of this Plan.
     */
    public void removeSubscriber(Customer subscriber) {
        if (subscriber.getSubscription() != this) {
            throw new IllegalArgumentException("The Customer is not a subscriber of this Plan.");
        }

        subscriber.setSubscription(null);
    }

    /**
     * Returns the set of Options that can be added to a subscription to this Plan.
     * <p>
     * The {@link Set} returned by this method cannot be modified directly, use the provided
     * {@link #addAvailableOption(Option)} and {@link #removeAvailableOption(Option)} methods to update the association.
     *
     * @return the set of {@link Option}s that can be added to a subscription to this Plan.
     */
    public Set<Option> getAvailableOptions() {
        return Collections.unmodifiableSet(availableOptions);
    }

    void _addAvailableOption(Option option) {
        availableOptions.add(option);
    }

    void _removeAvailableOption(Option option) {
        availableOptions.remove(option);
    }

    /**
     * Adds an Option to the set of available options for this Plan.
     *
     * @param option the Option that is available to subscribers of this Plan.
     */
    public void addAvailableOption(Option option) {
        option.setPlan(this);
    }

    /**
     * Removes an Option from the set of available options for this Plan.
     *
     * @param option the Option that should be removed from this Plan.
     * @throws IllegalArgumentException if the Option is not an available option of this Plan.
     */
    public void removeAvailableOption(Option option) {
        if (option.getPlan() != this) {
            throw new IllegalArgumentException("The Option is not an availableOption of this Plan.");
        }

        option.setPlan(null);
    }

    @Override
    public boolean attributesEqual(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plan)) return false;

        Plan plan = (Plan) o;
        return Objects.equals(getId(), plan.getId()) &&
                Objects.equals(getName(), plan.getName()) &&
                Objects.equals(getDescription(), plan.getDescription()) &&
                getBillingPeriod() == plan.getBillingPeriod() &&
                BigDecimals.equalsIgnoringScale(getRate(), plan.getRate()) &&
                Associations.associationEquals(getSubscribers(), plan.getSubscribers()) &&
                Associations.associationEquals(getAvailableOptions(), plan.getAvailableOptions());
    }

}
