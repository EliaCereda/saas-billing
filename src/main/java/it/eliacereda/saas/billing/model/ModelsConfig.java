package it.eliacereda.saas.billing.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.CurrentDateTimeProvider;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/** Spring Configuration class that enables the support for JPA Auditing used by {@link Auditable}. */
@Configuration
@EnableJpaAuditing(dateTimeProviderRef = "dateTimeProvider")
public class ModelsConfig {
    /**
     * Explicitly defines the instance of DateTimeProvider that JPA Auditing should use to update
     * {@link Auditable#getCreatedDate()} and {@link Auditable#getModifiedDate()}. This allows the test suite to access
     * the same time source used by JPA.
     * @return a DateTimeProvider instance.
     */
    @Bean
    public DateTimeProvider dateTimeProvider() { return CurrentDateTimeProvider.INSTANCE; }
}
