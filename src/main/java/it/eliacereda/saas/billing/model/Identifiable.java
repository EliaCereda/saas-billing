package it.eliacereda.saas.billing.model;

/**
 * Retrieves the identifier of any JPA entity.
 * <p>
 * Any entity to be stored in the database should have a primary key, this interface allows to retrieve it without
 * knowing the specific entity that is being processed. This is primarily used by the test suite to fetch entities from
 * their repositories by identifier.
 *
 * @param <ID> the type of the identifier attribute
 */
public interface Identifiable<ID> {

    /**
     * Returns the identifier of this entity.
     *
     * @return the identifier of this entity. Can be {@code null}.
     */
    ID getId();
}
