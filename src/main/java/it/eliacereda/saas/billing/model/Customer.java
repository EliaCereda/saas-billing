package it.eliacereda.saas.billing.model;

import it.eliacereda.saas.billing.util.Associations;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Abstract entity that models an user that subscribes to this service. It stores the association with the current
 * subscription Plan and with the additional Options linked to the subscription, while the concrete subclasses store
 * the billing information for the different kinds of customers.
 *
 * @see Person
 * @see Organization
 */
@Entity
@Table(name = "customers")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Customer extends Auditable<Long> {
    @Id
    @GeneratedValue(generator = "customer_generator")
    @SequenceGenerator(
            name = "customer_generator",
            sequenceName = "customer_sequence",
            initialValue = 1000
    )
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Plan subscription;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(joinColumns = { @JoinColumn(name = "subscriber_id") }, inverseJoinColumns = { @JoinColumn(name = "option_id") })
    private Set<Option> activeOptions = new HashSet<>();

    // Getters and Setters

    public Long getId() {
        return id;
    }

    /** Sets the identifier of this entity. This method is used in the test suite and shouldn't be used in normal code. */
    void setId(Long id) {
        this.id = id;
    }

    // Associations

    /**
     * Returns the Plan that this Customer is currently subscribed to.
     *
     * @return the current subscription Plan.
     */
    public Plan getSubscription() {
        return subscription;
    }

    /**
     * Sets the Plan that this Customer is currently subscribed to.
     *
     * @param subscription the new subscription Plan.
     */
    public void setSubscription(Plan subscription) {
        if (this.subscription != null) {
            this.subscription._removeSubscriber(this);
        }

        this.subscription = subscription;

        if (this.subscription != null) {
            this.subscription._addSubscriber(this);
        }
    }

    /**
     * Returns the set of additional Options that this Customer added to its subscription.
     * <p>
     * The {@link Set} returned by this method cannot be modified directly, use the provided
     * {@link #addActiveOption(Option)} and {@link #removeActiveOption(Option)} methods to update the association.
     * @return the set of {@link Option}s that this Customer added to the subscription Plan.
     */
    public Set<Option> getActiveOptions() {
        return Collections.unmodifiableSet(activeOptions);
    }

    /**
     * Adds an Option to the set of active additional Options.
     *
     * @param option the Option that should be added to the subscription of this Customer.
     */
    public void addActiveOption(Option option) {
        activeOptions.add(option);
        option._addSubscriber(this);
    }


    /**
     * Removes an Option from the set of active additional Options.
     *
     * @param option the Option that should be removed from the subscription of this Customer.
     */
    public void removeActiveOption(Option option) {
        activeOptions.remove(option);
        option._removeSubscriber(this);
    }

    // AttributesComparable

    @Override
    public boolean attributesEqual(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;

        Customer customer = (Customer) o;
        return Objects.equals(getId(), customer.getId()) &&
                Associations.associationEquals(getSubscription(), customer.getSubscription()) &&
                Associations.associationEquals(getActiveOptions(), customer.getActiveOptions());
    }
}
