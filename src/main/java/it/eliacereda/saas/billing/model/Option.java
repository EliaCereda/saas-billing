package it.eliacereda.saas.billing.model;

import it.eliacereda.saas.billing.util.Associations;
import it.eliacereda.saas.billing.util.BigDecimals;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Entity that models an optional feature that can be added to a subscription {@link Plan} for a given price.
 */
@Entity
@Table(name = "options")
public class Option extends Auditable<Long> {
    @Id
    @GeneratedValue(generator = "option_generator")
    @SequenceGenerator(
            name = "option_generator",
            sequenceName = "option_sequence",
            initialValue = 1000
    )
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Plan plan;

    @Column(nullable = false)
    private String name;

    @Lob
    @Column(nullable = false)
    private String description;

    @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal rate;

    /** The Set of Option instances that this instance depends on. */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(joinColumns = { @JoinColumn(name = "dependant_id") }, inverseJoinColumns = { @JoinColumn(name = "dependency_id") })
    private Set<Option> dependencies = new HashSet<>();

    /** The Set of Option instances that depend on this instance. */
    @ManyToMany(mappedBy = "dependencies", fetch = FetchType.LAZY)
    private Set<Option> dependants = new HashSet<>();

    /** The Set of subscribers that have activated this Option. */
    @ManyToMany(mappedBy = "activeOptions", fetch = FetchType.LAZY)
    private Set<Customer> subscribers = new HashSet<>();

    // Getters and Setters

    public Long getId() {
        return id;
    }

    /** Sets the identifier of this entity. This method is used in the test suite and shouldn't be used in normal code. */
    void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the name of this Option.
     * @return the name of this Option.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this Option.
     * @param name the new name of this Option.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets a long description of this Option.
     * @return the long description of this Option.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the long description of this Option.
     * @param description the new long description of this Option.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the amount billed to subscribers of this Option. Options follow the billing period of their parent Plan.
     * @return the amount billed for this Option.
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the amount billed to subscribers of this Option. Options follow the billing period of their parent Plan.
     * @param rate the new amount billed for this Option.
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    // Associations

    /**
     * Returns the Plan that this Option belongs to.
     *
     * @return the parent subscription Plan.
     */
    public Plan getPlan() {
        return plan;
    }

    /**
     * Sets the Plan that this Option belongs to.
     *
     * @param plan the subscription Plan that this Option should belong to.
     */
    public void setPlan(Plan plan) {
        if (this.plan != null) {
            this.plan._removeAvailableOption(this);
        }

        this.plan = plan;

        if (this.plan != null) {
            this.plan._addAvailableOption(this);
        }
    }

    /**
     * Returns the set of Options that this Option depends on to be activated. A Customer that wants to activate this
     * Option must also enable all its dependencies.
     * <p>
     * The {@link Set} returned by this method cannot be modified directly, use the provided
     * {@link #addDependency(Option)} and {@link #removeDependency(Option)} methods to update the association.
     *
     * @return the set of {@link Option}s that this Option depends upon to be activated.
     */
    public Set<Option> getDependencies() {
        return Collections.unmodifiableSet(dependencies);
    }

    /**
     * Adds an Option for this Option to depend on.
     *
     * @param dependency the Option that this Option should depend on.
     * @throws IllegalArgumentException if trying to add this Option as a dependency on itself.
     */
    public void addDependency(Option dependency) {
        if (dependency == this) {
            throw new IllegalArgumentException("An Option cannot depend on itself.");
        }

        dependencies.add(dependency);
        dependency._addDependant(this);
    }

    /**
     * Removes an Option from those that this Option depends on.
     *
     * @param dependency the Option that this Option should not depend on anymore.
     */
    public void removeDependency(Option dependency) {
        dependencies.remove(dependency);
        dependency._removeDependant(this);
    }

    /**
     * Returns the set of Options that depend on this Option. A Customer that wants to activate any of the dependant
     * Options must also enable this Option.
     * <p>
     * The {@link Set} returned by this method cannot be modified directly, use the provided
     * {@link #addDependant(Option)} and {@link #removeDependant(Option)} methods to update the association.
     *
     * @return the set of {@link Option}s that that depend upon this Option to be activated.
     */
    public Set<Option> getDependants() {
        return Collections.unmodifiableSet(dependants);
    }

    private void _addDependant(Option dependant) {
        dependants.add(dependant);
    }

    private void _removeDependant(Option dependant) {
        dependants.remove(dependant);
    }

    /**
     * Adds an Option for this Option to depend on.
     *
     * @param dependant the Option that should depend on this Option.
     * @throws IllegalArgumentException if trying to add this Option as a dependency on itself.
     */
    public void addDependant(Option dependant) {
        dependant.addDependency(this);
    }

    /**
     * Removes an Option from those depend on this Option.
     *
     * @param dependant the Option that should not depend on this Option anymore.
     */
    public void removeDependant(Option dependant) {
        dependant.removeDependency(this);
    }

    /**
     * Returns the set of Customers that have activated this Option at the moment.
     * <p>
     * The {@link Set} returned by this method cannot be modified directly, use the provided
     * {@link #addSubscriber(Customer)} and {@link #removeSubscriber(Customer)} methods to update the association.
     *
     * @return the set of {@link Customer}s that have activated this Option.
     */
    public Set<Customer> getSubscribers() {
        return Collections.unmodifiableSet(subscribers);
    }

    void _addSubscriber(Customer subscriber) {
        subscribers.add(subscriber);
    }

    void _removeSubscriber(Customer subscriber) {
        subscribers.remove(subscriber);
    }

    /**
     * Adds a Customer to the set of subscribers that have activated this Option.
     *
     * @param subscriber the Customer that wants to activate this Option.
     */
    public void addSubscriber(Customer subscriber) {
        subscriber.addActiveOption(this);
    }

    /**
     * Removes a Customer from the set of subscribers that have activated this Option.
     *
     * @param subscriber the Customer that wants to deactivate this Option.
     */
    public void removeSubscriber(Customer subscriber) {
        subscriber.removeActiveOption(this);
    }

    // AttributesComparable

    @Override
    public boolean attributesEqual(Object o) {
        if (this == o) return true;
        if (!(o instanceof Option)) return false;

        Option option = (Option) o;
        return Objects.equals(getId(), option.getId()) &&
                Associations.associationEquals(getPlan(), option.getPlan()) &&
                Objects.equals(getName(), option.getName()) &&
                Objects.equals(getDescription(), option.getDescription()) &&
                BigDecimals.equalsIgnoringScale(getRate(), option.getRate()) &&
                Associations.associationEquals(getDependencies(), option.getDependencies()) &&
                Associations.associationEquals(getDependants(), option.getDependants()) &&
                Associations.associationEquals(getSubscribers(), option.getSubscribers());
    }

}
