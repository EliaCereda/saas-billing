package it.eliacereda.saas.billing;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SaaSBillingApplicationTests {

    /** Tests that Spring is initialized correctly. */
    @Test
    void contextLoads() {
    }

}
