package it.eliacereda.saas.billing.repository.association;

import it.eliacereda.saas.billing.model.Customer;
import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Option;
import it.eliacereda.saas.billing.repository.CustomerRepository;
import it.eliacereda.saas.billing.repository.OptionRepository;
import org.hibernate.TransientObjectException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static it.eliacereda.saas.billing.model.Assertions.assertAttributesEqual;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class ActiveOptionsAssociationTests {

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private EntityTestFactory<Option> optionFactory;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EntityTestFactory<Customer> customerFactory;

    /** Tests that adding an active option updates the subscriber side */
    @Test
    void testBidirectionalSynchronization_activeOptions() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        assertTrue(customer.getActiveOptions().isEmpty());
        assertTrue(option.getSubscribers().isEmpty());

        customer.addActiveOption(option);

        assertEquals(customer.getActiveOptions(), Set.of(option));
        assertEquals(option.getSubscribers(), Set.of(customer));

        customer.removeActiveOption(option);

        assertTrue(customer.getActiveOptions().isEmpty());
        assertTrue(option.getSubscribers().isEmpty());
    }

    /** Tests that adding a subscriber updates the active option side */
    @Test
    void testBidirectionalSynchronization_subscribers() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        assertTrue(customer.getActiveOptions().isEmpty());
        assertTrue(option.getSubscribers().isEmpty());

        option.addSubscriber(customer);

        assertEquals(customer.getActiveOptions(), Set.of(option));
        assertEquals(option.getSubscribers(), Set.of(customer));

        option.removeSubscriber(customer);

        assertTrue(customer.getActiveOptions().isEmpty());
        assertTrue(option.getSubscribers().isEmpty());
    }

    /** Tests that persisting an active option succeeds */
    @Test
    void testAddValidActiveOption() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        customer.addActiveOption(option);

        option = optionRepository.save(option);
        customer = customerRepository.save(customer);

        assertDoesNotThrow(() -> {
            optionRepository.flushAndClear();
            customerRepository.flushAndClear();
        });

        assertEquals(customer.getActiveOptions(), Set.of(option));
        assertEquals(option.getSubscribers(), Set.of(customer));

        var fetchedOption = optionRepository.findById(option.getId()).orElseThrow();
        var fetchedCustomer = customerRepository.findById(customer.getId()).orElseThrow();

        assertNotSame(option, fetchedOption);
        assertAttributesEqual(option, fetchedOption);

        assertNotSame(customer, fetchedCustomer);
        assertAttributesEqual(customer, fetchedCustomer);
    }

    /** Tests that persisting a subscriber succeeds */
    @Test
    void testAddValidSubscriber() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        option.addSubscriber(customer);

        option = optionRepository.save(option);
        customer = customerRepository.save(customer);

        assertDoesNotThrow(() -> {
            optionRepository.flushAndClear();
            customerRepository.flushAndClear();
        });

        assertEquals(customer.getActiveOptions(), Set.of(option));
        assertEquals(option.getSubscribers(), Set.of(customer));

        var fetchedOption = optionRepository.findById(option.getId()).orElseThrow();
        var fetchedCustomer = customerRepository.findById(customer.getId()).orElseThrow();

        assertNotSame(option, fetchedOption);
        assertAttributesEqual(option, fetchedOption);

        assertNotSame(customer, fetchedCustomer);
        assertAttributesEqual(customer, fetchedCustomer);
    }

    /**
     * Tests that deleting a Customer succeeds even if it has active options.
     */
    @Test
    void testCustomerDeleteWithActiveOptions() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        option.addSubscriber(customer);

        option = optionRepository.save(option);
        customer = customerRepository.save(customer);

        optionRepository.flush();

        customerRepository.delete(customer);

        assertDoesNotThrow(() -> customerRepository.flush());
    }

    /** Tests that deleting an Option fails if it has any subscribers. */
    @Test
    void testOptionDeleteWithSubscribers() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        option.addSubscriber(customer);

        option = optionRepository.save(option);
        customer = customerRepository.save(customer);

        optionRepository.flush();

        optionRepository.delete(option);

        var exception = assertThrows(DataIntegrityViolationException.class, () -> optionRepository.flush());
        var cause = (ConstraintViolationException)exception.getCause();
    }

    /**
     * Tests that deleting an Option succeeds if it has no subscribers.
     */
    @Test
    void testOptionSuccessfulDelete() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        option.addSubscriber(customer);

        option = optionRepository.save(option);
        customer = customerRepository.save(customer);

        option.removeSubscriber(customer);

        // NOTE: this appears to works even if the Option isn't explicitly saved.
        optionRepository.delete(option);

        assertDoesNotThrow(() -> optionRepository.flush());
    }

    /**
     * Tests that CustomerRepository throws an exception if you try to flush a Customer with an active option that hasn't been
     * persisted yet.
     */
    @Test
    void testAddTransientActiveOption() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        option.addSubscriber(customer);

        customer = customerRepository.save(customer);

        var exception = assertThrows(InvalidDataAccessApiUsageException.class, () -> {
            customerRepository.flush();
        });

        var firstCause = (IllegalStateException)exception.getCause();
        var cause = (TransientObjectException)firstCause.getCause();
    }

    /**
     * Tests that OptionRepository does not throw an exception if you try to flush an Option with a subscriber that
     * hasn't been persisted yet. Since the association is owned by the activeOptions side, it is not persisted.
     */
    @Test
    void testAddTransientSubscriber() {
        var customer = customerFactory.createValidEntity();
        var option = optionFactory.createValidEntity();

        option.addSubscriber(customer);

        option = optionRepository.save(option);

        assertDoesNotThrow(() -> optionRepository.flush());

        assertNull(customer.getId(), "customer should still be transient");
    }
}
