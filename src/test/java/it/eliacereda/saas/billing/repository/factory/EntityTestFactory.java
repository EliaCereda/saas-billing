package it.eliacereda.saas.billing.repository.factory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/** An interface used by the test suite to obtain and modify instances of the entities being tests. */
public interface EntityTestFactory<T> {

    /**
     * Creates a valid instance of the entity.
     *
     * The test suite use this method to create the dummy data it needs for its tests. populateMandatoryAssociations
     * allows to create instances with the mandatory associations not populated, so that test cases that excercise the
     * behavior of the association can populate them as they need.
     *
     * @param idx an index that may be used to satisfy uniqueness constraints when a test needs multiple instances.
     * @param populateMandatoryAssociations a flag that specifies whether the mandatory associations should be populated
     *                                      by the EntityTestFactory.
     */
    T createValidEntity(int idx, boolean populateMandatoryAssociations);

    /**
     * Updates a valid instance of the entity. Successive calls to this method should always produce a change in the
     * attributes.
     *
     * @param entity the instance to update.
     */
    void updateValidEntity(T entity);


    default T createValidEntity() {
        return createValidEntity(0, true);
    }

    default T createValidEntity(int idx) {
        return createValidEntity(idx, true);
    }

    default T createValidEntity(boolean populateMandatoryAssociations) {
        return createValidEntity(0, populateMandatoryAssociations);
    }

    default List<T> createValidEntities(int count, boolean populateMandatoryAssociations) {
        return IntStream
                .range(0, count)
                .mapToObj((idx) -> this.createValidEntity(idx, populateMandatoryAssociations))
                .collect(Collectors.toList());
    }

    default List<T> createValidEntities(int count) {
        return createValidEntities(count, true);
    }
}
