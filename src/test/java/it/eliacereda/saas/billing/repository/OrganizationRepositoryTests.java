package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Organization;
import it.eliacereda.saas.billing.repository.contract.AuditableRepositoryContract;
import it.eliacereda.saas.billing.repository.contract.ValidationRepositoryContract;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.auditing.DateTimeProvider;

import javax.persistence.NonUniqueResultException;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OrganizationRepositoryTests implements AuditableRepositoryContract<Organization, Long>, ValidationRepositoryContract<Organization, Long> {

    @Autowired
    private OrganizationRepository repository;

    @Autowired
    private DateTimeProvider timeProvider;

    @Autowired
    private EntityTestFactory<Organization> entityFactory;

    // BaseRepositoryContract

    @Override
    public OrganizationRepository getRepository() {
        return repository;
    }

    @Override
    public EntityTestFactory<Organization> getEntityFactory() {
        return entityFactory;
    }

    // AuditableRepositoryContract

    @Override
    public DateTimeProvider getTimeProvider() {
        return timeProvider;
    }

    // ValidationRepositoryContract

    @Override
    public Set<String> nullableAttributes() { return Set.of("subscription", "vatIdentifier", "organizationName"); }

    @Override
    public Set<String> unmodifiableSetAssociations() {
        return Set.of("activeOptions");
    }

    // Query Methods

    /** Tests that findByVatIdentifier correctly finds the Organization with a given identifier. */
    @Test
    void testFindByVatIdentifier() {
        var organizationA = new Organization();
        var organizationB = new Organization();
        var organizationC = new Organization();

        organizationA.setVatIdentifier("1");
        organizationA.setOrganizationName("Organization A");

        organizationB.setVatIdentifier("2");
        organizationB.setOrganizationName("Organization B");

        organizationC.setVatIdentifier("2");
        organizationC.setOrganizationName("Organization C");

        repository.save(organizationA);
        repository.save(organizationB);
        repository.save(organizationC);

        var fetchedOrganization = repository.findByVatIdentifier("1").orElseThrow();

        assertEquals(organizationA, fetchedOrganization);

        var exception = assertThrows(IncorrectResultSizeDataAccessException.class, () -> repository.findByVatIdentifier("2"));

        assertTrue(exception.getCause() instanceof NonUniqueResultException);

        var nonExistentOrganization = repository.findByVatIdentifier("3");

        assertEquals(Optional.empty(), nonExistentOrganization);
    }
}
