package it.eliacereda.saas.billing.repository.factory;

import it.eliacereda.saas.billing.model.Organization;
import org.springframework.stereotype.Component;

@Component
public class OrganizationTestFactory implements EntityTestFactory<Organization> {
    @Override
    public Organization createValidEntity(int idx, boolean populateMandatoryAssociations) {
        var entity = new Organization();

        entity.setVatIdentifier("IT09876543210");
        entity.setOrganizationName("Test Organization " + idx);

        return entity;
    }

    @Override
    public void updateValidEntity(Organization entity) {
        var vatIdentifier = entity.getVatIdentifier();
        var nationCode = vatIdentifier.substring(0, 2);
        var identifier = vatIdentifier.substring(2);

        var newIdentifier = Long.parseLong(identifier) + 1;

        entity.setVatIdentifier(nationCode + newIdentifier);
        entity.setOrganizationName(entity.getOrganizationName() + " modified");
    }
}
