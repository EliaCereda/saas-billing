package it.eliacereda.saas.billing.repository.association;

import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Option;
import it.eliacereda.saas.billing.repository.OptionRepository;
import org.hibernate.TransientObjectException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static it.eliacereda.saas.billing.model.Assertions.assertAttributesEqual;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class DependenciesAssociationTests {

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private EntityTestFactory<Option> optionFactory;

    /** Tests that adding a dependency updates the dependants side */
    @Test
    void testBidirectionalSynchronization_dependencies() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        assertTrue(option0.getDependencies().isEmpty());
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertTrue(option1.getDependants().isEmpty());

        option0.addDependency(option1);

        assertEquals(option0.getDependencies(), Set.of(option1));
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertEquals(option1.getDependants(), Set.of(option0));

        option0.removeDependency(option1);

        assertTrue(option0.getDependencies().isEmpty());
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertTrue(option1.getDependants().isEmpty());
    }

    /** Tests that adding a dependant updates the dependencies side */
    @Test
    void testBidirectionalSynchronization_dependants() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        assertTrue(option0.getDependencies().isEmpty());
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertTrue(option1.getDependants().isEmpty());

        option1.addDependant(option0);

        assertEquals(option0.getDependencies(), Set.of(option1));
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertEquals(option1.getDependants(), Set.of(option0));

        option1.removeDependant(option0);

        assertTrue(option0.getDependencies().isEmpty());
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertTrue(option1.getDependants().isEmpty());
    }

    /** Tests that persisting a dependency succeeds */
    @Test
    void testAddValidDependency() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option0.addDependency(option1);

        option0 = optionRepository.save(option0);
        option1 = optionRepository.save(option1);

        assertDoesNotThrow(() -> {
            optionRepository.flushAndClear();
        });

        assertEquals(option0.getDependencies(), Set.of(option1));
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertEquals(option1.getDependants(), Set.of(option0));

        var fetchedOption0 = optionRepository.findById(option0.getId()).orElseThrow();
        var fetchedOption1 = optionRepository.findById(option1.getId()).orElseThrow();

        assertNotSame(option0, fetchedOption0);
        assertAttributesEqual(option0, fetchedOption0);

        assertNotSame(option1, fetchedOption1);
        assertAttributesEqual(option1, fetchedOption1);
    }

    /** Tests that persisting a dependant succeeds */
    @Test
    void testAddValidDependant() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option1.addDependant(option0);

        option0 = optionRepository.save(option0);
        option1 = optionRepository.save(option1);

        assertDoesNotThrow(() -> {
            optionRepository.flushAndClear();
        });

        assertEquals(option0.getDependencies(), Set.of(option1));
        assertTrue(option0.getDependants().isEmpty());
        assertTrue(option1.getDependencies().isEmpty());
        assertEquals(option1.getDependants(), Set.of(option0));

        var fetchedOption0 = optionRepository.findById(option0.getId()).orElseThrow();
        var fetchedOption1 = optionRepository.findById(option1.getId()).orElseThrow();

        assertNotSame(option0, fetchedOption0);
        assertAttributesEqual(option0, fetchedOption0);

        assertNotSame(option1, fetchedOption1);
        assertAttributesEqual(option1, fetchedOption1);
    }

    /** Tests that an error is thrown if trying to add a self dependency */
    @Test
    void testAddSelfDependency() {
        var option = optionFactory.createValidEntity();

        assertThrows(IllegalArgumentException.class, () -> option.addDependency(option));
        assertThrows(IllegalArgumentException.class, () -> option.addDependant(option));
    }

    /**
     * Tests that deleting an Option succeeds if it has no dependencies.
     */
    @Test
    void testOptionSuccessfulDelete() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option0.addDependency(option1);

        option0 = optionRepository.save(option0);
        option1 = optionRepository.save(option1);

        option0.removeDependency(option1);

        // NOTE: it appears that this works even if the Customer isn't explicitly saved.
        optionRepository.delete(option0);

        assertDoesNotThrow(() -> optionRepository.flush());
    }

    /**
     * Tests that deleting an Option succeeds if it has dependencies but no dependants.
     */
    @Test
    void testOptionDeleteWithDependencies() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option0.addDependency(option1);

        option0 = optionRepository.save(option0);
        option1 = optionRepository.save(option1);

        optionRepository.flush();

        optionRepository.delete(option0);

        assertDoesNotThrow(() -> optionRepository.flush());
    }

    /** Tests that deleting an Option fails if it has any dependants, since they cannot exist without it. */
    @Test
    void testOptionDeleteWithDependants() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option1.addDependant(option0);

        option0 = optionRepository.save(option0);
        option1 = optionRepository.save(option1);

        optionRepository.delete(option1);

        var exception = assertThrows(DataIntegrityViolationException.class, () -> optionRepository.flush());
        var cause = (ConstraintViolationException)exception.getCause();
    }

    /**
     * Tests that OptionRepository throws an exception if you try to flush an Option with a dependency that hasn't been
     * persisted yet.
     */
    @Test
    void testAddTransientDependency() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option0.addDependency(option1);

        option0 = optionRepository.save(option0);

        var exception = assertThrows(InvalidDataAccessApiUsageException.class, () -> {
            optionRepository.flush();
        });

        var firstCause = (IllegalStateException)exception.getCause();
        var cause = (TransientObjectException)firstCause.getCause();
    }

    /**
     * Tests that OptionRepository does not throw an exception if you try to flush an Option with a dependant that
     * hasn't been persisted yet. Since the association is owned by the dependency side, it is not persisted.
     */
    @Test
    void testAddTransientDependant() {
        var option0 = optionFactory.createValidEntity(0);
        var option1 = optionFactory.createValidEntity(1);

        option0.addDependency(option1);

        option1 = optionRepository.save(option1);

        assertDoesNotThrow(() -> optionRepository.flush());

        assertNull(option0.getId(), "option0 should still be transient");
    }
}
