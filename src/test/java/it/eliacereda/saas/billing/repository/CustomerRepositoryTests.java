package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.Customer;
import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.repository.contract.AuditableRepositoryContract;
import it.eliacereda.saas.billing.repository.contract.ValidationRepositoryContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.auditing.DateTimeProvider;

import java.util.Set;

@SpringBootTest
class CustomerRepositoryTests implements AuditableRepositoryContract<Customer, Long>, ValidationRepositoryContract<Customer, Long> {

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private DateTimeProvider timeProvider;

    @Autowired
    private EntityTestFactory<Customer> entityFactory;

    // BaseRepositoryContract

    @Override
    public CustomerRepository getRepository() {
        return repository;
    }

    @Override
    public EntityTestFactory<Customer> getEntityFactory() {
        return entityFactory;
    }

    // AuditableRepositoryContract

    @Override
    public DateTimeProvider getTimeProvider() {
        return timeProvider;
    }

    // ValidationRepositoryContract

    @Override
    public Set<String> nullableAttributes() { return Set.of("subscription"); }

    @Override
    public Set<String> unmodifiableSetAssociations() {
        return Set.of("activeOptions");
    }
}
