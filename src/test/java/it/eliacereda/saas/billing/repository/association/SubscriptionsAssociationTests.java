package it.eliacereda.saas.billing.repository.association;

import it.eliacereda.saas.billing.model.Customer;
import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Plan;
import it.eliacereda.saas.billing.repository.CustomerRepository;
import it.eliacereda.saas.billing.repository.PlanRepository;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static it.eliacereda.saas.billing.model.Assertions.assertAttributesEqual;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class SubscriptionsAssociationTests {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private EntityTestFactory<Customer> customerFactory;

    @Autowired
    private EntityTestFactory<Plan> planFactory;

    /** Tests that adding a subscription from the Customer side updates the Plan side */
    @Test
    void testBidirectionalSynchronization_Customer() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        assertNull(customer.getSubscription());
        assertTrue(plan.getSubscribers().isEmpty());

        customer.setSubscription(plan);

        assertEquals(customer.getSubscription(), plan);
        assertEquals(plan.getSubscribers(), Set.of(customer));

        customer.setSubscription(null);

        assertNull(customer.getSubscription());
        assertTrue(plan.getSubscribers().isEmpty());
    }

    /** Tests that adding a subscription from the Plan side updates the Customer side */
    @Test
    void testBidirectionalSynchronization_Plan() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        assertNull(customer.getSubscription());
        assertTrue(plan.getSubscribers().isEmpty());

        plan.addSubscriber(customer);

        assertEquals(customer.getSubscription(), plan);
        assertEquals(plan.getSubscribers(), Set.of(customer));

        plan.removeSubscriber(customer);

        assertNull(customer.getSubscription());
        assertTrue(plan.getSubscribers().isEmpty());
    }

    /** Tests that persisting a subscription added from the Customer side succeeds */
    @Test
    void testAddValidSubscription_Customer() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        customer.setSubscription(plan);

        // NOTE: the order matters.
        // If the save operations are swapped, Hibernate firsts INSERTs the Customer with subscription_id=NULL, then
        // it INSERTs the Plan and finally it UPDATEs the Customer with the generated Plan id. Saving the Plan first
        // saves an UPDATE statement.
        plan = planRepository.save(plan);
        customer = customerRepository.save(customer);

        assertDoesNotThrow(() -> {
            planRepository.flushAndClear();
            customerRepository.flushAndClear();
        });

        assertEquals(customer.getSubscription(), plan);
        assertEquals(plan.getSubscribers(), Set.of(customer));

        var fetchedPlan = planRepository.findById(plan.getId()).orElseThrow();
        var fetchedCustomer = customerRepository.findById(customer.getId()).orElseThrow();

        assertNotSame(plan, fetchedPlan);
        assertAttributesEqual(plan, fetchedPlan);

        assertNotSame(customer, fetchedCustomer);
        assertAttributesEqual(customer, fetchedCustomer);
    }

    /** Tests that persisting a subscription added from the Plan side succeeds */
    @Test
    void testAddValidSubscription_Plan() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        plan.addSubscriber(customer);

        // NOTE: the order matters.
        // If the save operations are swapped, Hibernate firsts INSERTs the Customer with subscription_id=NULL, then
        // it INSERTs the Plan and finally it UPDATEs the Customer with the generated Plan id. Saving the Plan first
        // saves an UPDATE statement.
        plan = planRepository.save(plan);
        customer = customerRepository.save(customer);

        assertDoesNotThrow(() -> {
            planRepository.flushAndClear();
            customerRepository.flushAndClear();
        });

        assertEquals(customer.getSubscription(), plan);
        assertEquals(plan.getSubscribers(), Set.of(customer));

        var fetchedPlan = planRepository.findById(plan.getId()).orElseThrow();
        var fetchedCustomer = customerRepository.findById(customer.getId()).orElseThrow();

        assertNotSame(plan, fetchedPlan);
        assertAttributesEqual(plan, fetchedPlan);

        assertNotSame(customer, fetchedCustomer);
        assertAttributesEqual(customer, fetchedCustomer);
    }

    /**
     * Tests that CustomerRepository throws an exception if you try to flush a Customer with a subscription to a Plan
     * that hasn't been persisted yet.
     */
    @Test
    void testAddSubscriptionWithTransientPlan() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        customer.setSubscription(plan);

        customer = customerRepository.save(customer);

        var exception = assertThrows(InvalidDataAccessApiUsageException.class, () -> {
            customerRepository.flush();
        });

        var firstCause = (IllegalStateException)exception.getCause();
        var cause = (TransientPropertyValueException)firstCause.getCause();

        assertEquals(cause.getTransientEntityName(), plan.getClass().getName());
        assertEquals(cause.getPropertyOwnerEntityName(), customer.getClass().getName());
        assertEquals(cause.getPropertyName(), "subscription");
    }

    /**
     * Tests that PlanRepository does not throw an exception if you try to flush a Plan with a subscription by a Customer
     * that hasn't been persisted yet. Since the foreign key is on the Customer side, the subscription is not persisted.
     */
    @Test
    void testAddSubscriptionWithTransientCustomer() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        plan.addSubscriber(customer);

        plan = planRepository.save(plan);

        assertDoesNotThrow(() -> planRepository.flush());

        assertNull(customer.getId(), "The Customer should still be transient");
    }

    /**
     * Tests that deleting a Plan succeeds if it has no subscribers.
     */
    @Test
    void testPlanSuccessfulDelete() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        customer.setSubscription(plan);

        plan = planRepository.save(plan);
        customer = customerRepository.save(customer);

        customer.setSubscription(null);

        // NOTE: it appears that this works even if the Customer isn't explicitly saved.
        planRepository.delete(plan);

        assertDoesNotThrow(() -> planRepository.flush());
    }

    /**
     * Tests enforcement of the foreign key constraint on subscription_id. In particular it should not be possible
     * to delete a Plan if it has any subscribers, since they should each be migrated to a new Plan prior to deleting
     * the old one.
     */
    @Test
    void testPlanDeleteWithSubscribers() {
        var customer = customerFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        customer.setSubscription(plan);

        plan = planRepository.save(plan);
        customer = customerRepository.save(customer);

        planRepository.delete(plan);

        var exception = assertThrows(DataIntegrityViolationException.class, () -> planRepository.flush());
        var cause = (ConstraintViolationException)exception.getCause();
    }

    /**
     * Tests that removeSubscriber fails if the Customer is not a subscriber of the given Plan.
     */
    @Test
    void testPlanRemoveSubscriberFromDifferentPlan() {
        var customer = customerFactory.createValidEntity();
        var plan0 = planFactory.createValidEntity(0);
        var plan1 = planFactory.createValidEntity(1);

        assertNull(customer.getSubscription());

        assertThrows(IllegalArgumentException.class, () -> plan0.removeSubscriber(customer));
        assertNull(customer.getSubscription());

        customer.setSubscription(plan0);

        assertThrows(IllegalArgumentException.class, () -> plan1.removeSubscriber(customer));
        assertEquals(customer.getSubscription(), plan0);

        assertDoesNotThrow(() -> plan0.removeSubscriber(customer));
        assertNull(customer.getSubscription());
    }
}
