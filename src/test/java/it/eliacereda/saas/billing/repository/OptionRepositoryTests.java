package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Option;
import it.eliacereda.saas.billing.repository.contract.AuditableRepositoryContract;
import it.eliacereda.saas.billing.repository.contract.ValidationRepositoryContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.auditing.DateTimeProvider;

import java.util.Set;

@SpringBootTest
class OptionRepositoryTests implements AuditableRepositoryContract<Option, Long>, ValidationRepositoryContract<Option, Long> {

    @Autowired
    private OptionRepository repository;

    @Autowired
    private DateTimeProvider timeProvider;

    @Autowired
    private EntityTestFactory<Option> entityFactory;

    // BaseRepositoryContract

    @Override
    public OptionRepository getRepository() {
        return repository;
    }

    @Override
    public EntityTestFactory<Option> getEntityFactory() {
        return entityFactory;
    }

    // AuditableRepositoryContract

    @Override
    public DateTimeProvider getTimeProvider() {
        return timeProvider;
    }

    // ValidationRepositoryContract

    @Override
    public Set<String> nonNullableAttributes() {
        return Set.of("plan", "name", "description", "rate");
    }

    @Override
    public Set<String> unmodifiableSetAssociations() {
        return Set.of("dependencies", "dependants", "subscribers");
    }
}
