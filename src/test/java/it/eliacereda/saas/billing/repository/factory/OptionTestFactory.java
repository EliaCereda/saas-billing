package it.eliacereda.saas.billing.repository.factory;

import it.eliacereda.saas.billing.model.Option;
import it.eliacereda.saas.billing.model.Plan;
import it.eliacereda.saas.billing.repository.PlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class OptionTestFactory implements EntityTestFactory<Option> {

    @Autowired
    private EntityTestFactory<Plan> planFactory;

    @Autowired
    private PlanRepository planRepository;

    @Override
    public Option createValidEntity(int idx, boolean populateMandatoryAssociations) {
        var option = new Option();

        if (populateMandatoryAssociations) {
            var plan = planFactory.createValidEntity(idx);

            planRepository.save(plan);

            option.setPlan(plan);
        }

        option.setName("Test Option " + idx);
        option.setDescription("An Option created for the test suite");
        option.setRate(BigDecimal.TEN);

        return option;
    }

    @Override
    public void updateValidEntity(Option entity) {
        entity.setName(entity.getName() + " modified");
        entity.setDescription(entity.getDescription() + " modified");
        entity.setRate(entity.getRate().add(BigDecimal.ONE));
    }
}
