package it.eliacereda.saas.billing.repository.factory;

import it.eliacereda.saas.billing.model.Person;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class PersonTestFactory implements EntityTestFactory<Person> {
    @Override
    public Person createValidEntity(int idx, boolean populateMandatoryAssociations) {
        var entity = new Person();

        entity.setGivenName("No. " + idx);
        entity.setFamilyName("Test Person");
        entity.setDateOfBirth(LocalDate.of(1996, 3, 14));

        return entity;
    }

    @Override
    public void updateValidEntity(Person entity) {
        entity.setGivenName(entity.getGivenName() + " modified");
        entity.setFamilyName(entity.getFamilyName() + " modified");
        entity.setDateOfBirth(entity.getDateOfBirth().plusDays(1));
    }
}
