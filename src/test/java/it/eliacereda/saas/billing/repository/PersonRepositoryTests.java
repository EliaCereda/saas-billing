package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Person;
import it.eliacereda.saas.billing.repository.contract.AuditableRepositoryContract;
import it.eliacereda.saas.billing.repository.contract.ValidationRepositoryContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.auditing.DateTimeProvider;

import java.util.Set;

@SpringBootTest
class PersonRepositoryTests implements AuditableRepositoryContract<Person, Long>, ValidationRepositoryContract<Person, Long> {

    @Autowired
    private PersonRepository repository;

    @Autowired
    private DateTimeProvider timeProvider;

    @Autowired
    private EntityTestFactory<Person> entityFactory;

    // BaseRepositoryContract

    @Override
    public PersonRepository getRepository() {
        return repository;
    }

    @Override
    public EntityTestFactory<Person> getEntityFactory() {
        return entityFactory;
    }

    // AuditableRepositoryContract

    @Override
    public DateTimeProvider getTimeProvider() {
        return timeProvider;
    }

    // ValidationRepositoryContract

    @Override
    public Set<String> nullableAttributes() { return Set.of("subscription", "givenName", "familyName", "dateOfBirth"); }

    @Override
    public Set<String> unmodifiableSetAssociations() {
        return Set.of("activeOptions");
    }
}
