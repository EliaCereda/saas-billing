package it.eliacereda.saas.billing.repository;

import it.eliacereda.saas.billing.model.BillingPeriod;
import it.eliacereda.saas.billing.model.Organization;
import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Plan;
import it.eliacereda.saas.billing.repository.contract.AuditableRepositoryContract;
import it.eliacereda.saas.billing.repository.contract.ValidationRepositoryContract;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.auditing.DateTimeProvider;

import javax.persistence.NonUniqueResultException;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PlanRepositoryTests implements AuditableRepositoryContract<Plan, Long>, ValidationRepositoryContract<Plan, Long> {

    @Autowired
    private PlanRepository repository;

    @Autowired
    private DateTimeProvider timeProvider;

    @Autowired
    private EntityTestFactory<Plan> entityFactory;

    // BaseRepositoryContract

    @Override
    public PlanRepository getRepository() {
        return repository;
    }

    @Override
    public EntityTestFactory<Plan> getEntityFactory() {
        return entityFactory;
    }

    // AuditableRepositoryContract

    @Override
    public DateTimeProvider getTimeProvider() {
        return timeProvider;
    }

    // ValidationRepositoryContract

    @Override
    public Set<String> nonNullableAttributes() {
        return Set.of("name", "description", "rate", "billingPeriod");
    }

    @Override
    public Set<String> unmodifiableSetAssociations() {
        return Set.of("subscribers", "availableOptions");
    }

    // Query Methods

    /** Tests that findAllByBillingPeriod correctly finds the Plans with a given billing period. */
    @Test
    void testFindAllByBillingPeriod() {
        var planA = new Plan();
        var planB = new Plan();
        var planC = new Plan();

        planA.setName("Plan A");
        planA.setDescription("Description");
        planA.setBillingPeriod(BillingPeriod.YEARLY);
        planA.setRate(BigDecimal.TEN);

        planB.setName("Plan B");
        planB.setDescription("Description");
        planB.setBillingPeriod(BillingPeriod.MONTHLY);
        planB.setRate(BigDecimal.TEN);

        planC.setName("Plan C");
        planC.setDescription("Description");
        planC.setBillingPeriod(BillingPeriod.MONTHLY);
        planC.setRate(BigDecimal.TEN);

        repository.saveAll(Set.of(planA, planB, planC));

        var yearlyPlans = repository.findAllByBillingPeriod(BillingPeriod.YEARLY);

        assertEquals(Set.of(planA), yearlyPlans);

        var monthlyPlans = repository.findAllByBillingPeriod(BillingPeriod.MONTHLY);

        assertEquals(Set.of(planB, planC), monthlyPlans);

        var weeklyPlans = repository.findAllByBillingPeriod(BillingPeriod.WEEKLY);

        assertEquals(Set.of(), weeklyPlans);
    }
}
