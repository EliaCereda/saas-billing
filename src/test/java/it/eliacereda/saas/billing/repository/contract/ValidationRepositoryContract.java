package it.eliacereda.saas.billing.repository.contract;

import it.eliacereda.saas.billing.model.AttributesComparable;
import it.eliacereda.saas.billing.model.Identifiable;
import org.hibernate.PropertyValueException;
import org.hibernate.id.IdentifierGenerationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.jpa.JpaSystemException;

import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test suite that exercises the validation of attribute values for the entity being tested.
 * <p>
 * At the moment it supports two kind of validations:
 * <ul>
 * <li>It allows to specify the nullability of each attribute and it verifies that this is enforced by the entity.</li>
 * <li>It allows to specify whether each association should be directly modifiable through the Set object returned to
 *     clients and it verifies that this is enforced by the entity.</li>
 * </ul>
 */
public interface ValidationRepositoryContract<T extends Identifiable<ID> & AttributesComparable, ID> extends BaseRepositoryContract<T, ID> {

    /** Returns the set of nullable attributes of the entity being tested. */
    default Set<String> nullableAttributes() { return Set.of(); }

    /** Returns the set of non-nullable attributes of the entity being tested. */
    default Set<String> nonNullableAttributes() { return Set.of(); }

    /** Returns the set of associations that can be directly modified. */
    default Set<String> modifiableSetAssociations() { return Set.of(); }

    /** Returns the set of associations that should only be modified through methods of the entity being tested. */
    default Set<String> unmodifiableSetAssociations() { return Set.of(); }

    /// Utility methods

    private ParameterizedType findThisInterface(Class<?> cls) {
        var interfaces = cls.getGenericInterfaces();

        for (var interf: interfaces) {
            if (interf instanceof ParameterizedType) {
                var type = (ParameterizedType)interf;

                if (type.getRawType() == ValidationRepositoryContract.class) {
                    return type;
                }
            }
        }

        var superClass = cls.getSuperclass();

        if (superClass != null) {
            return findThisInterface(superClass);
        } else {
            return null;
        }
    }

    private Class<T> getEntityClass() {
        var testClass = getClass();
        var contractInterface = findThisInterface(testClass);

        assert contractInterface != null;

        var entityClass = contractInterface.getActualTypeArguments()[0];

        //noinspection unchecked
        return (Class<T>)entityClass;
    }

    private Set<String> getSettableAttributes() {
        var entityClass = getEntityClass();
        var beanInfo = assertDoesNotThrow(() -> Introspector.getBeanInfo(entityClass));
        var properties = beanInfo.getPropertyDescriptors();

        return Arrays.stream(properties)
                .filter(pd -> (pd.getWriteMethod() != null)) // Filter settable attributes
                .map(FeatureDescriptor::getName) // Map their names
                .collect(Collectors.toSet());
    }

    private Object getAttribute(T entity, String attribute) {
        return PropertyAccessorFactory.forBeanPropertyAccess(entity).getPropertyValue(attribute);
    }

    private void setAttribute(T entity, String attribute, Object value) {
        PropertyAccessorFactory.forBeanPropertyAccess(entity).setPropertyValue(attribute, value);
    }

    private Set<String> getAssociationsOfType(Class<?> associationType) {
        var entityClass = getEntityClass();
        var beanInfo = assertDoesNotThrow(() -> Introspector.getBeanInfo(entityClass));
        var properties = beanInfo.getPropertyDescriptors();

        return Arrays.stream(properties)
                .filter(pd -> (pd.getReadMethod() != null && pd.getPropertyType() == associationType)) // Filter gettable attributes of a given type
                .map(FeatureDescriptor::getName) // Map their names
                .collect(Collectors.toSet());
    }

    ///  Test cases

    /**
     * Tests that the union of nullableAttributes() and nonNullableAttributes() matches the set of all settable
     * attributes of the entity being tested.
     */
    @Test
    default void validateNullabilityAnnotations() {
        var attributes = getSettableAttributes();

        Set<String> annotatedAttributes = new HashSet<>();

        annotatedAttributes.addAll(nullableAttributes());
        annotatedAttributes.addAll(nonNullableAttributes());

        assertEquals(attributes, annotatedAttributes, "All attributes must be marked nullable or non-nullable");
    }

    /** Tests that each attribute returned by nullableAttributes() can be successfully set to null. */
    @Test
    default void validateNullableConstraints() {
        var repository = getRepository();
        var entityFactory = getEntityFactory();

        for (var attribute: nullableAttributes()) {
            var entity = entityFactory.createValidEntity();

            setAttribute(entity, attribute, null);

            assertDoesNotThrow(() -> repository.save(entity));
        }
    }

    /** Tests that each attribute returned by nonNullableAttributes() throws an exception if set to null. */
    @Test
    default void validateNonNullableConstraints() {
        var repository = getRepository();
        var entityFactory = getEntityFactory();

        for (var attribute: nonNullableAttributes()) {
            var entity = entityFactory.createValidEntity();

            setAttribute(entity, attribute, null);

            var exception = assertThrows(RuntimeException.class, () -> repository.save(entity));

            if (exception instanceof DataIntegrityViolationException) {
                var nestedException = (PropertyValueException)exception.getCause();

                assertEquals(nestedException.getPropertyName(), attribute);
            } else if (exception instanceof JpaSystemException) {
                var nestedException = (IdentifierGenerationException)exception.getCause();
            } else {
                fail("The exception should be an instance of DataIntegrityViolationException or JpaSystemException.");
            }
        }
    }

    /**
     * Tests that the union of modifiableSetAssociations() and unmodifiableSetAssociations() matches the set of all
     * Set&lt;T&gt; associations of the entity being tested.
     */
    @Test
    default void validateModifiableAnnotations() {
        var associations = getAssociationsOfType(Set.class);

        Set<String> annotatedAssociations = new HashSet<>();

        annotatedAssociations.addAll(modifiableSetAssociations());
        annotatedAssociations.addAll(unmodifiableSetAssociations());

        assertEquals(associations, annotatedAssociations, "All associations of type Set<T> must be marked modifiable or unmodifiable");
    }

    /** Tests that each association returned by modifiableSetAssociations() can be modified directly. */
    @Test
    default void validateModifiableAssociations() {
        var entityFactory = getEntityFactory();

        for (var associationName: modifiableSetAssociations()) {
            var entity = entityFactory.createValidEntity();
            var association = (Set<?>)getAttribute(entity, associationName);

            assertDoesNotThrow(() -> association.remove(null));
            assertDoesNotThrow(() -> association.add(null));
        }
    }

    /** Tests that each association returned by unmodifiableSetAssociations() throws an exception if modified directly. */
    @Test
    default void validateUnmodifiableAssociations() {
        var entityFactory = getEntityFactory();

        for (var associationName: unmodifiableSetAssociations()) {
            var entity = entityFactory.createValidEntity();
            var association = (Set<?>)getAttribute(entity, associationName);

            assertThrows(UnsupportedOperationException.class, () -> association.remove(null));
            assertThrows(UnsupportedOperationException.class, () -> association.add(null));
        }
    }
}
