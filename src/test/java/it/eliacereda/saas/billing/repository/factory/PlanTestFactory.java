package it.eliacereda.saas.billing.repository.factory;

import it.eliacereda.saas.billing.model.BillingPeriod;
import it.eliacereda.saas.billing.model.Plan;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class PlanTestFactory implements EntityTestFactory<Plan> {
    @Override
    public Plan createValidEntity(int idx, boolean populateMandatoryAssociations) {
        var plan = new Plan();

        plan.setName("Test Plan " + idx);
        plan.setDescription("A Plan created for the test suite");

        plan.setRate(BigDecimal.valueOf(100.00));
        plan.setBillingPeriod(BillingPeriod.YEARLY);

        return plan;
    }

    @Override
    public void updateValidEntity(Plan entity) {
        entity.setName(entity.getName() + " modified");
        entity.setDescription(entity.getDescription() + " modified");
        entity.setRate(entity.getRate().add(BigDecimal.ONE));
    }
}
