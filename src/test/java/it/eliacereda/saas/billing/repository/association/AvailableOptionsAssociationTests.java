package it.eliacereda.saas.billing.repository.association;

import it.eliacereda.saas.billing.repository.factory.EntityTestFactory;
import it.eliacereda.saas.billing.model.Option;
import it.eliacereda.saas.billing.model.Plan;
import it.eliacereda.saas.billing.repository.OptionRepository;
import it.eliacereda.saas.billing.repository.PlanRepository;
import org.hibernate.TransientPropertyValueException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static it.eliacereda.saas.billing.model.Assertions.assertAttributesEqual;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class AvailableOptionsAssociationTests {

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private EntityTestFactory<Plan> planFactory;

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private EntityTestFactory<Option> optionFactory;

    /**  Tests that adding an association from the Option side updates the Plan side */
    @Test
    void testBidirectionalSynchronization_Option() {
        var plan = planFactory.createValidEntity();
        var option = optionFactory.createValidEntity(false);

        assertNull(option.getPlan());
        assertTrue(plan.getAvailableOptions().isEmpty());

        option.setPlan(plan);

        assertEquals(option.getPlan(), plan);
        assertEquals(plan.getAvailableOptions(), Set.of(option));

        option.setPlan(null);

        assertNull(option.getPlan());
        assertTrue(plan.getAvailableOptions().isEmpty());
    }

    /** Tests that adding an association from the Plan side updates the Option side */
    @Test
    void testBidirectionalSynchronization_Plan() {
        var plan = planFactory.createValidEntity();
        var option = optionFactory.createValidEntity(false);

        assertNull(option.getPlan());
        assertTrue(plan.getAvailableOptions().isEmpty());

        plan.addAvailableOption(option);

        assertEquals(option.getPlan(), plan);
        assertEquals(plan.getAvailableOptions(), Set.of(option));

        plan.removeAvailableOption(option);

        assertNull(option.getPlan());
        assertTrue(plan.getAvailableOptions().isEmpty());
    }

    /** Tests that persisting an association added from the Option side succeeds */
    @Test
    void testAddValidAssociation_Option() {
        var plan = planFactory.createValidEntity();
        var option = optionFactory.createValidEntity(false);

        option.setPlan(plan);

        plan = planRepository.save(plan);
        option = optionRepository.save(option);

        assertDoesNotThrow(() -> {
            planRepository.flushAndClear();
            optionRepository.flushAndClear();
        });

        assertEquals(option.getPlan(), plan);
        assertEquals(plan.getAvailableOptions(), Set.of(option));

        var fetchedPlan = planRepository.findById(plan.getId()).orElseThrow();
        var fetchedOption = optionRepository.findById(option.getId()).orElseThrow();

        assertNotSame(plan, fetchedPlan);
        assertAttributesEqual(plan, fetchedPlan);

        assertNotSame(option, fetchedOption);
        assertAttributesEqual(option, fetchedOption);
    }

    /** Tests that persisting an association added from the Plan side succeeds  */
    @Test
    void testAddValidAssociation_Plan() {
        var plan = planFactory.createValidEntity();
        var option = optionFactory.createValidEntity(false);

        plan.addAvailableOption(option);

        plan = planRepository.save(plan);
        option = optionRepository.save(option);

        assertDoesNotThrow(() -> {
            planRepository.flushAndClear();
            optionRepository.flushAndClear();
        });

        assertEquals(option.getPlan(), plan);
        assertEquals(plan.getAvailableOptions(), Set.of(option));

        var fetchedPlan = planRepository.findById(plan.getId()).orElseThrow();
        var fetchedOption = optionRepository.findById(option.getId()).orElseThrow();

        assertNotSame(plan, fetchedPlan);
        assertAttributesEqual(plan, fetchedPlan);

        assertNotSame(option, fetchedOption);
        assertAttributesEqual(option, fetchedOption);
    }

    /**
     * Tests that deleting a Plan also deletes its availableOptions.
     */
    @Test
    void testPlanCascadeDelete() {
        var plan = planFactory.createValidEntity();
        var option = optionFactory.createValidEntity(false);

        plan.addAvailableOption(option);

        plan = planRepository.save(plan);
        option = optionRepository.save(option);

        var id = option.getId();

        assertTrue(optionRepository.existsById(id));

        planRepository.delete(plan);

        assertDoesNotThrow(() -> planRepository.flush());
        assertFalse(optionRepository.existsById(id));
    }

    /**
     * Tests that OptionRepository throws an exception if you try to flush an Option with an association to a Plan
     * that hasn't been persisted yet.
     */
    @Test
    void testAddAssociationWithTransientPlan() {
        var option = optionFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        option.setPlan(plan);

        // NOTE: this differs from the equivalent test in SubscriptionsAssociationTests because Option.plan is
        // non-nullable. It appears that Hibernate validates transient non-nullable attributes during save(entity) while
        // nullable attributes are validated at flush() time.
        var exception = assertThrows(InvalidDataAccessApiUsageException.class, () -> {
            optionRepository.save(option);
        });

        var firstCause = (IllegalStateException)exception.getCause();
        var cause = (TransientPropertyValueException)firstCause.getCause();

        assertEquals(cause.getTransientEntityName(), plan.getClass().getName());
        assertEquals(cause.getPropertyOwnerEntityName(), option.getClass().getName());
        assertEquals(cause.getPropertyName(), "plan");
    }

    /**
     * Tests that PlanRepository does not throw an exception if you try to flush a Plan with an association to an Option
     * that hasn't been persisted yet. Since the foreign key is on the Option side, the association is not persisted.
     */
    @Test
    void testAddAssociationWithTransientCustomer() {
        var option = optionFactory.createValidEntity();
        var plan = planFactory.createValidEntity();

        plan.addAvailableOption(option);

        plan = planRepository.save(plan);

        assertDoesNotThrow(() -> planRepository.flush());

        assertNull(option.getId(), "The Option should still be transient");
    }

    /**
     * Tests that removeAvailableOption fails if the Option does not belong to the given Plan.
     */
    @Test
    void testPlanRemoveSubscriberFromDifferentPlan() {
        var option = optionFactory.createValidEntity(false);
        var plan0 = planFactory.createValidEntity(0);
        var plan1 = planFactory.createValidEntity(1);

        assertNull(option.getPlan());

        assertThrows(IllegalArgumentException.class, () -> plan0.removeAvailableOption(option));
        assertNull(option.getPlan());

        option.setPlan(plan0);

        assertThrows(IllegalArgumentException.class, () -> plan1.removeAvailableOption(option));
        assertEquals(option.getPlan(), plan0);

        assertDoesNotThrow(() -> plan0.removeAvailableOption(option));
        assertNull(option.getPlan());
    }
}
