package it.eliacereda.saas.billing.repository.contract;

import it.eliacereda.saas.billing.model.Auditable;
import org.junit.jupiter.api.Test;
import org.springframework.data.auditing.DateTimeProvider;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test suite that exercises the behavior of Auditable&lt;T&gt;. It ensures that createdDate and modifiedDate are
 * updated correctly when creating and modifying an instance and that the timestamps are consistent.
 */
public interface AuditableRepositoryContract<T extends Auditable<ID>, ID> extends BaseRepositoryContract<T, ID> {

    /**
     * Returns the DateTimeProvider used by JPA Auditing to update createdDate and modifiedDate. This allows to compare
     * them with the correct current time in the tests.
     */
    DateTimeProvider getTimeProvider();

    private LocalDateTime getNow() {
        return LocalDateTime.from(getTimeProvider().getNow().orElseThrow());
    }

    /** Tests that createdDate and modifiedDate behave correctly when creating a new entity. */
    @Test
    default void auditableEntityCreation() {
        var beforeCreationDate = getNow();

        var repository = getRepository();
        var entityFactory = getEntityFactory();

        var entity = entityFactory.createValidEntity();

        var createdDate = entity.getCreatedDate();
        var modifiedDate = entity.getModifiedDate();

        assertNull(createdDate, "createdDate should not be set until the instance has been saved");
        assertNull(modifiedDate, "modifiedDate should not be set until the instance has been saved");

        entity = repository.save(entity);

        createdDate = entity.getCreatedDate();
        modifiedDate = entity.getModifiedDate();

        assertNotNull(createdDate, "createdDate should be set after the instance has been saved");
        assertNotNull(modifiedDate, "modifiedDate should be set after the instance has been saved");

        var afterCreationDate = getNow();

        assertTrue(beforeCreationDate.isBefore(afterCreationDate),
                "The LocalDateTimes returned by getNow() should increase");

        assertTrue(beforeCreationDate.isBefore(createdDate),
                "beforeCreationDate should be before createdDate");
        assertTrue(createdDate.isBefore(afterCreationDate),
                "createdDate should be before afterCreationDate");

        assertTrue(beforeCreationDate.isBefore(modifiedDate),
                "beforeCreationDate should be before modifiedDate");
        assertTrue(modifiedDate.isBefore(afterCreationDate),
                "modifiedDate should be before afterCreationDate");
    }

    /** Tests that createdDate and modifiedDate behave correctly when modifying an existing entity. */
    @Test
    default void auditableEntityModification() {
        var repository = getRepository();
        var entityFactory = getEntityFactory();

        var entity = entityFactory.createValidEntity();

        entity = repository.save(entity);

        var previousCreatedDate = entity.getCreatedDate();
        var previousModifiedDate = entity.getModifiedDate();

        var beforeModificationDate = getNow();

        entity = repository.saveAndFlush(entity);

        var createdDate = entity.getCreatedDate();
        var modifiedDate = entity.getModifiedDate();

        assertTrue(previousCreatedDate.isEqual(createdDate),
                "createdDate should stay the same if no changes have been made to the entity");
        assertTrue(previousModifiedDate.isEqual(modifiedDate),
                "modifiedDate should stay the same if no changes have been made to the entity");

        entityFactory.updateValidEntity(entity);

        entity = repository.saveAndFlush(entity);

        var afterModificationDate = getNow();

        assertTrue(beforeModificationDate.isBefore(afterModificationDate),
                "The LocalDateTimes returned by getNow should increase");

        createdDate = entity.getCreatedDate();
        modifiedDate = entity.getModifiedDate();

        assertTrue(previousCreatedDate.isEqual(createdDate),
                "createdDate should stay the same");
        assertTrue(previousModifiedDate.isBefore(modifiedDate),
                "modifiedDate should increase");

        assertTrue(beforeModificationDate.isBefore(modifiedDate),
                "beforeModificationDate should be before modifiedDate");
        assertTrue(modifiedDate.isBefore(afterModificationDate),
                "modifiedDate should be before afterModificationDate");
    }
}
