package it.eliacereda.saas.billing.repository.factory;

import it.eliacereda.saas.billing.model.Customer;
import it.eliacereda.saas.billing.model.Organization;
import it.eliacereda.saas.billing.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerTestFactory implements EntityTestFactory<Customer> {

    @Autowired
    private EntityTestFactory<Person> personFactory;

    @Autowired
    private EntityTestFactory<Organization> organizationFactory;

    @Override
    public Customer createValidEntity(int idx, boolean populateMandatoryAssociations) {
        if (idx % 2 == 0) {
            return personFactory.createValidEntity(idx);
        } else {
            return organizationFactory.createValidEntity(idx);
        }
    }

    @Override
    public void updateValidEntity(Customer entity) {
        if (entity instanceof Person) {
            personFactory.updateValidEntity((Person)entity);
        } else if (entity instanceof Organization) {
            organizationFactory.updateValidEntity((Organization)entity);
        } else {
            throw new IllegalArgumentException("CustomerTestFactory does not support the supplied Customer subclass");
        }
    }
}
