package it.eliacereda.saas.billing.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class OrganizationTests extends CustomerTests {

    private Organization entity0;
    private Organization entity1;

    @Override
    public Organization getEntity0() {
        return entity0;
    }

    @Override
    public Organization getEntity1() {
        return entity1;
    }

    @BeforeEach
    void createEntities() {
        entity0 = new Organization();
        entity1 = new Organization();
    }

    @Override
    void testAttributesEqual_valid() {
        entity0.setVatIdentifier("IT09876543210");
        entity0.setOrganizationName("Organization Name");

        entity1.setVatIdentifier("IT09876543210");
        entity1.setOrganizationName("Organization Name");

        super.testAttributesEqual_valid();
    }

    @Test
    void testAttributesEqual_differentVatIdentifier() {
        entity0.setVatIdentifier("IT09876543210");
        entity1.setVatIdentifier("IT09876543211");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentOrganizationName() {
        entity0.setOrganizationName("Organization Name 0");
        entity1.setOrganizationName("Organization Name 1");

        assertFalse(entity0.attributesEqual(entity1));
    }
}
