package it.eliacereda.saas.billing.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class PersonTests extends CustomerTests {

    private Person entity0;
    private Person entity1;

    @Override
    public Person getEntity0() {
        return entity0;
    }

    @Override
    public Person getEntity1() {
        return entity1;
    }

    @BeforeEach
    void createEntities() {
        entity0 = new Person();
        entity1 = new Person();
    }

    @Override
    void testAttributesEqual_valid() {
        entity0.setGivenName("Given Name");
        entity0.setFamilyName("Family Name");
        entity0.setDateOfBirth(LocalDate.of(1996, 3, 14));

        entity1.setGivenName("Given Name");
        entity1.setFamilyName("Family Name");
        entity1.setDateOfBirth(LocalDate.of(1996, 3, 14));

        super.testAttributesEqual_valid();
    }

    @Test
    void testAttributesEqual_differentGivenName() {
        entity0.setGivenName("Given Name 0");
        entity1.setGivenName("Given Name 1");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentFamilyName() {
        entity0.setFamilyName("Family Name 0");
        entity1.setFamilyName("Family Name 1");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentDateOfBirth() {
        entity0.setDateOfBirth(LocalDate.of(2000, 1, 1));
        entity1.setDateOfBirth(LocalDate.of(2000, 1, 2));

        assertFalse(entity0.attributesEqual(entity1));
    }
}
