package it.eliacereda.saas.billing.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

abstract class CustomerTests {

    protected abstract Customer getEntity0();
    protected abstract Customer getEntity1();

    @Test
    void testAttributesEqual_default() {
        var entity0 = getEntity0();
        var entity1 = getEntity1();

        assertTrue(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_same() {
        var entity0 = getEntity0();

        assertTrue(entity0.attributesEqual(entity0));
    }

    @Test
    void testAttributesEqual_differentClass() {
        var entity0 = getEntity0();

        var fakeComparable = new AttributesComparable() {
            @Override
            public boolean attributesEqual(Object o) {
                return false;
            }
        };

        assertFalse(entity0.attributesEqual(fakeComparable));
    }

    @Test
    void testAttributesEqual_differentSubclass() {
        var entity0 = getEntity0();

        class FakeCustomer extends Customer {}

        var fakeCustomer = new FakeCustomer();

        assertFalse(entity0.attributesEqual(fakeCustomer));
    }

    @Test
    void testAttributesEqual_valid() {
        var planA = new Plan();
        var planB = new Plan();

        planA.setId(0L);
        planB.setId(0L);

        var optionA = new Option();
        var optionB = new Option();

        optionA.setId(0L);
        optionB.setId(0L);

        var entity0 = getEntity0();
        var entity1 = getEntity1();

        entity0.setId(0L);
        entity0.setSubscription(planA);
        entity0.addActiveOption(optionA);

        entity1.setId(0L);
        entity1.setSubscription(planB);
        entity1.addActiveOption(optionA);

        assertTrue(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentId() {
        var entity0 = getEntity0();
        var entity1 = getEntity1();

        entity0.setId(0L);
        entity1.setId(1L);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentSubscription() {
        var planA = new Plan();
        var planB = new Plan();

        planA.setId(0L);
        planB.setId(1L);

        var entity0 = getEntity0();
        var entity1 = getEntity1();

        entity0.setSubscription(planA);
        entity1.setSubscription(planB);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentActiveOptions() {
        var optionA = new Option();
        var optionB = new Option();

        optionA.setId(0L);
        optionB.setId(1L);

        var entity0 = getEntity0();
        var entity1 = getEntity1();

        entity0.addActiveOption(optionA);
        entity1.addActiveOption(optionB);

        assertFalse(entity0.attributesEqual(entity1));
    }
}