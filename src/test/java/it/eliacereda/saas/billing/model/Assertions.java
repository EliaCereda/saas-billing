package it.eliacereda.saas.billing.model;

import static org.junit.jupiter.api.Assertions.*;

public class Assertions {

    public static <T extends AttributesComparable> void assertAttributesEqual(T expected, T actual) {
        assertTrue(actual.attributesEqual(expected));
    }

    public static <T extends AttributesComparable> void assertAttributesNotEqual(T expected, T actual) {
        assertFalse(actual.attributesEqual(expected));
    }
}
