package it.eliacereda.saas.billing.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PlanTests {

    private Plan entity0;
    private Plan entity1;

    @BeforeEach
    void createEntities() {
        entity0 = new Plan();
        entity1 = new Plan();
    }

    @Test
    void testAttributesEqual_default() {
        assertTrue(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_same() {
        assertTrue(entity0.attributesEqual(entity0));
    }

    @Test
    void testAttributesEqual_differentClass() {
        var fakeComparable = new AttributesComparable() {
            @Override
            public boolean attributesEqual(Object o) {
                return false;
            }
        };

        assertFalse(entity0.attributesEqual(fakeComparable));
    }

    @Test
    void testAttributesEqual_valid() {
        var subscriberA = new Person();
        var subscriberB = new Person();

        subscriberA.setId(0L);
        subscriberB.setId(0L);

        var optionA = new Option();
        var optionB = new Option();

        optionA.setId(0L);
        optionB.setId(0L);

        entity0.setId(0L);
        entity0.setName("Name");
        entity0.setDescription("Description");
        entity0.setBillingPeriod(BillingPeriod.MONTHLY);
        entity0.setRate(BigDecimal.TEN);
        entity0.addSubscriber(subscriberA);
        entity0.addAvailableOption(optionA);

        entity1.setId(0L);
        entity1.setName("Name");
        entity1.setDescription("Description");
        entity1.setBillingPeriod(BillingPeriod.MONTHLY);
        entity1.setRate(BigDecimal.TEN);
        entity1.addSubscriber(subscriberB);
        entity1.addAvailableOption(optionB);

        assertTrue(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentId() {
        entity0.setId(0L);
        entity1.setId(1L);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentName() {
        entity0.setName("Name 0");
        entity1.setName("Name 1");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentDescription() {
        entity0.setDescription("Description 0");
        entity1.setDescription("Description 1");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentBillingPeriod() {
        entity0.setBillingPeriod(BillingPeriod.WEEKLY);
        entity1.setBillingPeriod(BillingPeriod.YEARLY);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentRate() {
        entity0.setRate(BigDecimal.ONE);
        entity1.setRate(BigDecimal.TEN);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentSubscribers() {
        var subscriberA = new Person();
        var subscriberB = new Person();

        subscriberA.setId(0L);
        subscriberB.setId(1L);

        entity0.addSubscriber(subscriberA);
        entity1.addSubscriber(subscriberB);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentAvailableOptions() {
        var optionA = new Option();
        var optionB = new Option();

        optionA.setId(0L);
        optionB.setId(1L);

        entity0.addAvailableOption(optionA);
        entity1.addAvailableOption(optionB);

        assertFalse(entity0.attributesEqual(entity1));
    }
}
