package it.eliacereda.saas.billing.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class OptionTests {

    private Option entity0;
    private Option entity1;

    @BeforeEach
    void createEntities() {
        entity0 = new Option();
        entity1 = new Option();
    }

    @Test
    void testAttributesEqual_default() {
        assertTrue(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_same() {
        assertTrue(entity0.attributesEqual(entity0));
    }

    @Test
    void testAttributesEqual_differentClass() {
        var fakeComparable = new AttributesComparable() {
            @Override
            public boolean attributesEqual(Object o) {
                return false;
            }
        };

        assertFalse(entity0.attributesEqual(fakeComparable));
    }

    @Test
    void testAttributesEqual_valid() {
        var planA = new Plan();
        var planB = new Plan();

        planA.setId(0L);
        planB.setId(0L);

        var dependencyA = new Option();
        var dependencyB = new Option();

        dependencyA.setId(0L);
        dependencyB.setId(0L);

        var dependantA = new Option();
        var dependantB = new Option();

        dependantA.setId(0L);
        dependantB.setId(0L);

        var subscriberA = new Person();
        var subscriberB = new Person();

        subscriberA.setId(0L);
        subscriberB.setId(0L);

        entity0.setId(0L);
        entity0.setPlan(planA);
        entity0.setName("Name");
        entity0.setDescription("Description");
        entity0.setRate(BigDecimal.TEN);
        entity0.addDependency(dependencyA);
        entity0.addDependant(dependantA);
        entity0.addSubscriber(subscriberA);

        entity1.setId(0L);
        entity1.setPlan(planB);
        entity1.setName("Name");
        entity1.setDescription("Description");
        entity1.setRate(BigDecimal.TEN);
        entity1.addDependency(dependencyB);
        entity1.addDependant(dependantB);
        entity1.addSubscriber(subscriberB);

        assertTrue(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentId() {
        entity0.setId(0L);
        entity1.setId(1L);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentPlan() {
        var planA = new Plan();
        var planB = new Plan();

        planA.setId(0L);
        planB.setId(1L);

        entity0.setPlan(planA);
        entity1.setPlan(planB);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentName() {
        entity0.setName("Name 0");
        entity1.setName("Name 1");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentDescription() {
        entity0.setDescription("Description 0");
        entity1.setDescription("Description 1");

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentRate() {
        entity0.setRate(BigDecimal.ONE);
        entity1.setRate(BigDecimal.TEN);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentDependencies() {
        var dependencyA = new Option();
        var dependencyB = new Option();

        dependencyA.setId(0L);
        dependencyB.setId(1L);

        entity0.addDependency(dependencyA);
        entity1.addDependency(dependencyB);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentDependants() {
        var dependantA = new Option();
        var dependantB = new Option();

        dependantA.setId(0L);
        dependantB.setId(1L);

        entity0.addDependant(dependantA);
        entity1.addDependant(dependantB);

        assertFalse(entity0.attributesEqual(entity1));
    }

    @Test
    void testAttributesEqual_differentSubscribers() {
        var subscriberA = new Person();
        var subscriberB = new Person();

        subscriberA.setId(0L);
        subscriberB.setId(1L);

        entity0.addSubscriber(subscriberA);
        entity1.addSubscriber(subscriberB);

        assertFalse(entity0.attributesEqual(entity1));
    }
}