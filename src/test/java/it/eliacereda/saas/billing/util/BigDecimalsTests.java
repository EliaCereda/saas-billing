package it.eliacereda.saas.billing.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BigDecimalsTests {

    /** Tests that equalsIgnoringScale behaves correctly with null values and BigDecimals of different scales. */
    @Test
    void testEqualsIgnoringScale() {
        assertTrue(BigDecimals.equalsIgnoringScale(null, null));
        assertTrue(BigDecimals.equalsIgnoringScale(BigDecimal.ONE, BigDecimal.ONE));
        assertFalse(BigDecimals.equalsIgnoringScale(BigDecimal.ONE, null));
        assertFalse(BigDecimals.equalsIgnoringScale(null, BigDecimal.ONE));

        var tenUnscaled = BigDecimal.valueOf(10, 0);
        var tenScaled = BigDecimal.valueOf(1, -1);

        assertNotEquals(tenUnscaled, tenScaled);
        assertTrue(BigDecimals.equalsIgnoringScale(tenUnscaled, tenScaled));
    }
}
