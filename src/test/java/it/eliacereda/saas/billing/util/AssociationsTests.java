package it.eliacereda.saas.billing.util;

import it.eliacereda.saas.billing.model.Identifiable;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AssociationsTests {

    private static class FakeIdentifiable implements Identifiable<Long> {
        private Long id;

        FakeIdentifiable(Long id) {
            this.id = id;
        }

        @Override
        public Long getId() {
            return id;
        }
    }

    /** Tests that associationEquals work correctly with single objects. */
    @Test
    void testAssociationEquals_single() {
        var identifiableA = new FakeIdentifiable(0L);
        var identifiableB = new FakeIdentifiable(0L);
        var identifiableC = new FakeIdentifiable(1L);
        var identifiableD = new FakeIdentifiable(null);
        var identifiableE = new FakeIdentifiable(null);

        assertTrue(Associations.associationEquals((FakeIdentifiable)null, null));
        assertFalse(Associations.associationEquals(identifiableA, null));
        assertFalse(Associations.associationEquals(null, identifiableA));
        assertTrue(Associations.associationEquals(identifiableA, identifiableA));
        assertTrue(Associations.associationEquals(identifiableA, identifiableB));
        assertFalse(Associations.associationEquals(identifiableA, identifiableC));

        assertFalse(Associations.associationEquals(identifiableD, null));
        assertFalse(Associations.associationEquals(null, identifiableD));
        assertFalse(Associations.associationEquals(identifiableA, identifiableD));
        assertTrue(Associations.associationEquals(identifiableD, identifiableE));
    }

    /** Tests that associationEquals work correctly with Sets of objects. */
    @Test
    void testAssociationEquals_Set() {
        var identifiableA = new FakeIdentifiable(0L);
        var identifiableB = new FakeIdentifiable(0L);
        var identifiableC = new FakeIdentifiable(1L);
        var identifiableD = new FakeIdentifiable(null);
        var identifiableE = new FakeIdentifiable(null);

        assertTrue(Associations.associationEquals((Set<FakeIdentifiable>)null, null));
        assertFalse(Associations.associationEquals(Set.of(identifiableA), null));
        assertFalse(Associations.associationEquals(null, Set.of(identifiableA)));
        assertTrue(Associations.associationEquals(Set.of(identifiableA), Set.of(identifiableA)));
        assertTrue(Associations.associationEquals(Set.of(identifiableA), Set.of(identifiableB)));
        assertFalse(Associations.associationEquals(Set.of(identifiableA), Set.of(identifiableC)));

        assertTrue(Associations.associationEquals(Set.of(identifiableD), Set.of(identifiableE)));

        assertTrue(Associations.associationEquals(Set.of(identifiableA, identifiableB), Set.of(identifiableA)));
        assertTrue(Associations.associationEquals(Set.of(identifiableD, identifiableE), Set.of(identifiableD)));
    }
}
