# SaaS Billing

[![pipeline status](https://gitlab.com/EliaCereda/saas-billing/badges/master/pipeline.svg)](https://gitlab.com/EliaCereda/saas-billing/commits/master)
[![coverage report](https://gitlab.com/EliaCereda/saas-billing/badges/master/coverage.svg)](https://eliacereda.gitlab.io/saas-billing/jacoco)

This project models a billing system for a Software-as-a-Service product. In particular, it supports managing customer 
identities, configuring the available subscription plans and the optional features that customers can add to their plan.

#### Repository

The source code is available on GitLab at <https://gitlab.com/EliaCereda/saas-billing>.

#### Contributors

This project has been developed by Elia Cereda (807539).

## Usage

Download the source code of the project by cloning the Git repository:

```bash
$ git clone https://gitlab.com/EliaCereda/saas-billing.git
$ cd saas-billing
```

Docker Compose can be used to quickly set up a development environment. The provided `docker-compose.yml` file contains 
the configuration to build the application and to start instances of PostgreSQL and pgAdmin. 

To run the test suite of the application, the recommended workflow is to first start the database and pgAdmin:

```bash
$ docker-compose up db db-admin
```

pgAdmin can be reached at <http://localhost:5433/> and it allows to directly interact with the database. See 
`docker-compose.yml` for the credentials and connection details.

Then, there are two methods to run the unit and integration tests of the application. The first is to use Docker 
Compose to build the application and run the tests in a container, with just a single command:

```bash
$ docker-compose run app mvn verify
```

Alternatively, it's possible to run the tests on the local machine. This requires a working installation of JDK 11 and 
Maven. The default configuration tries to connect to the PostgreSQL instance started by Docker Compose, edit 
`src/main/resources/application-default.properties` to connect to a different database.

```bash
$ mvn verify
```

Code Coverage information is automatically collected while running the test suite. It is also possible to generate the 
API Reference with the following command:

```bash
$ mvn javadoc:javadoc
```

The Code Coverage report can then be found in the `target/site/jacoco` folder, while the API Reference will be in the 
`target/site/apidocs` folder.

## Continuous Integration

This project uses GitLab CI to automatically run the unit and integration test suite for every commit that is pushed to 
the repository. The test results are visible on the [Commits](https://gitlab.com/EliaCereda/saas-billing/commits/master) 
page, while additional reports are published using GitLab Pages:
 
* The [Code Coverage report](https://eliacereda.gitlab.io/saas-billing/jacoco/), generated using Jacoco. It shows the 
coverage details of the latest commit on the `master` branch.
* The [API Reference](https://eliacereda.gitlab.io/saas-billing/apidocs/), generated with Javadoc.

## Model

This is the ER schema of the database used by the project.

![ER schema of the database](docs/er-schema.png)

The system is composed of 5 entities with 4 relations, including:

 * one self-relation, **Dependencies**; 
 * two lazy-loaded many-to-many relations, **Dependencies** and **Active Options**;
 * one inheritance hierarchy, composed of the **Customer**, **Person** and **Organization** classes;

All entities inherit from an `Auditable` abstract class that provides the `createdDate` and `modifiedDate` attributes 
using JPA Auditing. This allows to track the time of changes made from the Java application. 

In addition, `Auditable` implements two interfaces that are used by the test suite to work with entities generically: 
`Identifiable`, to access the primary key of any entity, and `AttributesComparable`, to compare the values of the 
attributes of two instances of an entity.

## Responsibilities

This section describes the responsibilities assigned to the various elements of the project. The base package is 
**`it.eliacereda.saas.billing`** and it is meant to be one part of the code of a Software-as-a-Service product.

Inside this package, the code is further divided as shown below. More information on the single components can be found
in the API Reference by following the links provided here.

* [`SaaSBillingApplication`][1] class, the main Spring Configuration class that initializes the application and loads the 
    other components through Component Scanning;
* [**`model`**][2] package: it contains the JPA entity classes used to map from relational records to Java objects. 
    - These are mostly POJOs that declare the mapping strategies to be applied using JPA annotations, without 
        implementing any complex behavior themselves. One exception is the [`AttributesComparable`][5] interface, that 
        all entities implement and allows to compare two instances of an entity to check that the values of their 
        attributes are equals.
    - [`Plan`][6] class: it models a subscription plan for this service.
    - [`Option`][7] class: it models an optional feature that can be added to a subscription plan for a given price.
    - [`Customer`][8] class: an abstract entity that models an user that subscribes to this service.
        - [`Person`][9] class: it models an individual that subscribes to this service.
        - [`Organization`][10] class: it models an organization that subscribes to this service.
* [**`repository`**][3] package: it contains the Spring Data repositories that abstract the specific operations used to 
    access the database from the rest of the project.
    - One repository interface is provided for each entity class, along with a [`BaseRepository`][11] interface that the 
        other inherit. 
    - `BaseRepository` provides a series of generic CRUD operations shared among all entities.
    - Each repository defines query methods specific to its entity (e.g. `findBy{SomeAttribute}OrderBy{AnotherAttribute}`). 
        At runtime, Spring Data automatically provides an implementation of these interfaces by parsing the name of each 
        method according to a [set of rules][12].
* [**`util`**][4] package: it contains utility classes for the implementation of [`AttributesComparable`][5].

 [1]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/SaaSBillingApplication.html
 [2]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/package-summary.html
 [3]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/repository/package-summary.html
 [4]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/util/package-summary.html
 [5]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/AttributesComparable.html
 [6]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/Plan.html
 [7]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/Option.html
 [8]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/Customer.html
 [9]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/Person.html
[10]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/model/Organization.html
[11]: https://eliacereda.gitlab.io/saas-billing/apidocs/it/eliacereda/saas/billing/repository/BaseRepository.html
[12]: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.details "Defining Query Methods"

The test suite is divided in multiple packages that reflect the structure of the main code:

* **`model`** package: it contains one test class for each entity. At the moment they exercise the correctness of the `AttributesComparable` implementations.
* **`repository`** package: it contains one test class for each repository, where there are a range of different tests.
    - Each repository test class contains test methods for the custom query methods provided by the corresponding 
    repository. The other tests are obtained by implementing the interfaces in the `contract` package.
    - **`contract`** package: it contains test interfaces to check the semantics that every repository must respect. 
        The test classes for concrete repositories implement these interfaces and inherit the test methods that they define.
        - `BaseRepositoryContract` interface: a test suite that exercises the expected behavior of all BaseRepositories.
            In particular, it tests the default CRUD operations provided by Spring Data JPA and the additional 
            operations defined by `BaseRepository`.
        - `ValidationRepositoryContract` interface: a test suite that exercises the validation of the attribute values of  
            an entity. At the moment it supports two kind of validations: attribute nullability and association modifiability.
        - `AuditableRepositoryContract` interface: a test suite that exercises the behavior of `Auditable<T>`.
    - **`association`** package: it contains one test class for each association between entities. Using a dedicated 
        class allows to easily test both sides of the associations together. 
    - **`factory`** package: it contains factory classes that create instances of the various entities, to be used by the test suite.
* **`util`** package: it contains tests for the classes in the corresponding package of the main code.

## Future developments

It would make sense to execute multiple runs of a few tests of the suite, providing various valid values and checking that 
they're all accepted by the persistence layer. JUnit allows to create [Parameterized Tests][parameterized], but they're
currently an experimental API that needs additional setup to be used. A possible future development would be to try and
use them in this context.

[parameterized]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests
